#pragma once
#include"CharactherCard.h"
#include<vector>
#include<algorithm>
#include<iostream>
//#include<iterator>

const unsigned int kInvNumberOfPlayers = 0;

class CharacterDeck
{
public:
	CharacterDeck();
	CharacterDeck(const CharacterDeck& otherDeck);
	CharacterDeck(CharacterDeck&& otherDeck);

public:
	CharacterDeck& operator = (const CharacterDeck& otherDeck);
	CharacterDeck& operator = (CharacterDeck&& otherDeck);

public:
	void generateCharactherDeck();
	void Shuffle();
	void printCards();
	void printBurntCards();
	CharacterDeck& getDeck();
	CharactherCard& getCard();
	unsigned getDeckSize() const;
	void getCertainCard(unsigned chIndex, CharactherCard& emptyCard);
	void eliminateFirstCardFromDeck();
	void eliminateCertainCardFromDeck(unsigned index);
	unsigned returnIndexOfCardInDeck(CharactherCard& card);
	std::vector<CharactherCard>::iterator firstCardInDeck();
	std::vector<CharactherCard>::iterator lastCardInDeck();
public:
	void addCard(const CharactherCard &card);
	void resetDeck();

protected:
	std::vector<CharactherCard> deckCards;
};




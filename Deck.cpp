#include "Deck.h"


Deck::Deck()
{
	numberOfPlayers = kInvalidNumberOfPlayers;
}

Deck::Deck(int nrOfPlayers)
{
	numberOfPlayers = nrOfPlayers;
}

Deck::Deck(const Deck & otherDeck)
{
	*this = otherDeck;
}

Deck::Deck(Deck && otherDeck)
{
	*this = std::move(otherDeck);
}

Deck & Deck::operator=(const Deck & otherDeck)
{
	numberOfPlayers = otherDeck.numberOfPlayers;
	deckCards = otherDeck.deckCards;
	return *this;
}

Deck & Deck::operator=(Deck && otherDeck)
{
	numberOfPlayers = std::move(otherDeck.numberOfPlayers);
	deckCards = std::move(otherDeck.deckCards);

	new(&otherDeck) Deck;

	return *this;
}

void Deck::generateMixedDeck()
{
	generateCharactherDeck();
	generateDistrictDeck();
}

void Deck::generateCharactherDeck()
{
	std::string names[] = { "Assassin", "Thief", "Magician", "King", "Bishop", "Merchant", "Architect", "Warlord" };

	for (uint8_t t = 1U; t <= 8U; ++t)
	{
		for (int i = 0; i < numberOfPlayers; ++i)
		{
			CharactherCard currentCard(t, names[t + 1], static_cast<CharactherCard::Type>(t));	//Type e public
			deckCards.push_back(currentCard);

		}
	}
}

void Deck::generateDistrictDeck()
{
	/* AM UNIFORMIZAT PRETUL CARTILOR DE ACEEASI CULOARE */
	std::string names[] = {
		"Manor", "Castle", "Palace",	//yellow,3
		"Temple", "Church", "Monastery", "Cathedral",	//blue,4
		"Tavern" ,"Market", "Tranding Post", "Docks", "Harbor", "Town Hall",	//green,6
		 "Watchtower", "Prison", "Battlefield", "Fortress",  //red,4
		 "Haunted City", "Keep", "Laboratory", "Smithy",	//purple,11
		"Graveyard", "Observatory", "School of Magic", "Library", "Great Wall", "University", "Dragon Gate" };


	/*YELLOW*/
	for (int i = 0; i <= 2; ++i)
	{
		for (int i = 0; i < numberOfPlayers; ++i)
		{
			DistrictCard currentCard(5, names[i], static_cast<DistrictCard::Color>(1U));
			deckCards.push_back(currentCard);

		}
	}

	/*BLUE*/
	for (int i = 3; i <= 6; ++i)
	{
		for (int i = 0; i < numberOfPlayers; ++i)
		{
			DistrictCard currentCard(4, names[i], static_cast<DistrictCard::Color>(2U));
			deckCards.push_back(currentCard);

		}
	}

	/*GREEN*/
	for (int i = 7; i <= 12; ++i)
	{
		for (int i = 0; i < numberOfPlayers; ++i)
		{
			DistrictCard currentCard(3, names[i], static_cast<DistrictCard::Color>(3U));
			deckCards.push_back(currentCard);

		}
	}

	/*RED*/
	for (int i = 13; i <= 16; ++i)
	{
		for (int i = 0; i < numberOfPlayers; ++i)
		{
			DistrictCard currentCard(4, names[i], static_cast<DistrictCard::Color>(4U));
			deckCards.push_back(currentCard);

		}
	}

	/*PURPLE*/
	for (int i = 17; i <= 29; ++i)
	{
		for (int i = 0; i < numberOfPlayers; ++i)
		{
			DistrictCard currentCard(2, names[i], static_cast<DistrictCard::Color>(5U));
			deckCards.push_back(currentCard);

		}
	}

}

void Deck::Shuffle()
{
	std::random_shuffle(deckCards.begin(), deckCards.end());
}

void Deck::printCards()
{
	std::cout << "Carti: ";
	//std::vector<Card>::iterator c;
	for (int i = 0 ; i < deckCards.size(); ++i)
		std::cout << deckCards[i].getName() << " ";
}

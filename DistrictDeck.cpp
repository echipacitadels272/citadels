#include "DistrictDeck.h"


DistrictDeck::DistrictDeck()
{
	numberOfPlayers = kInvalidNumberOfPlayers;
}

DistrictDeck::DistrictDeck(int nrOfPlayers)
{
	numberOfPlayers = nrOfPlayers;
}

DistrictDeck::DistrictDeck(const DistrictDeck & otherDeck)
{
	*this = otherDeck;
}

DistrictDeck::DistrictDeck(DistrictDeck && otherDeck)
{
	*this = std::move(otherDeck);
}

DistrictDeck & DistrictDeck::operator=(const DistrictDeck & otherDeck)
{
	numberOfPlayers = otherDeck.numberOfPlayers;
	deckCards = otherDeck.deckCards;
	return *this;
}

DistrictDeck & DistrictDeck::operator=(DistrictDeck && otherDeck)
{
	//numberOfPlayers = std::move(otherDeck.numberOfPlayers);
	deckCards = std::move(otherDeck.deckCards);

	new(&otherDeck) DistrictDeck;

	return *this;
}


void DistrictDeck::generateDistrictDeck()
{
	/* AM UNIFORMIZAT PRETUL CARTILOR DE ACEEASI CULOARE */
	std::string names[] = {
		"Manor", "Castle", "Palace", "Temple", "Church", "Monastery", "Cathedral", "Tavern" ,"Market", "Tranding Post", "Docks", "Harbor", "Town Hall",	"Watchtower", "Prison", "Battlefield", "Fortress", "Haunted City", "Keep", "Laboratory", "Smithy", "Graveyard", "Observatory", "School of Magic", "Library", "Great Wall", "University", "Dragon Gate" };

	for (unsigned j = 0; j < numberOfPlayers; ++j)
	{
		/*YELLOW*/
		for (unsigned i = 0; i <= 2; ++i)
		{
			DistrictCard currentCard(5, names[i], static_cast<DistrictCard::Color>(1U));
			deckCards.push_back(currentCard);
		}

		/*BLUE*/
		for (unsigned i = 3; i <= 6; ++i)
		{
			DistrictCard currentCard(4, names[i], static_cast<DistrictCard::Color>(2U));
			deckCards.push_back(currentCard);
		}

		/*GREEN*/
		for (unsigned i = 7; i <= 12; ++i)
		{
			DistrictCard currentCard(3, names[i], static_cast<DistrictCard::Color>(3U));
			deckCards.push_back(currentCard);
		}

		/*RED*/
		for (unsigned i = 13; i <= 16; ++i)
		{
			DistrictCard currentCard(4, names[i], static_cast<DistrictCard::Color>(4U));
			deckCards.push_back(currentCard);
		}

		/*PURPLE*/
		for (unsigned i = 17; i <= 27; ++i)
		{
			DistrictCard currentCard(2, names[i], static_cast<DistrictCard::Color>(5U));
			deckCards.push_back(currentCard);
		}
	}
}

void DistrictDeck::Shuffle()
{
	srand(time(0));
	std::random_shuffle(deckCards.begin(), deckCards.end());
}

void DistrictDeck::printCards()
{
	std::cout << "Carti: ";
	//std::vector<Card>::iterator c;
	for (unsigned i = 0 ; i < deckCards.size(); ++i)
		std::cout << deckCards[i].getName() << " ";
}

DistrictDeck DistrictDeck::getDeck()
{
	return *this;
}

DistrictCard DistrictDeck::getCard()
{
	cardToBeSent = deckCards.front();
	deckCards.erase(deckCards.begin());
	return cardToBeSent;
}

unsigned DistrictDeck::getDeckSize() const
{
	return deckCards.size();
}

void DistrictDeck::putCardBack(DistrictCard & cardToBePutBack)
{
	deckCards.push_back(cardToBePutBack);
	//Shuffle();
}


void DistrictDeck::insertDistrictCard(DistrictCard & cardToBeInserted)
{
	deckCards.push_back(cardToBeInserted);
}

unsigned DistrictDeck::returnGoldValueOfDeck()
{
	unsigned combinedGoldCost = 0;

	for (auto index : deckCards)
	{
		combinedGoldCost += index.GetCostInGold();
	}
	return combinedGoldCost;
}

unsigned DistrictDeck::getDeckSize()
{
	return deckCards.size();
}

bool DistrictDeck::containsFiveDistrictColors()
{
	unsigned distinctDistrictColors = 0;
	for (auto index : deckCards)
	{
		if (index.GetColor() == DistrictCard::Color::Blue)
			distinctDistrictColors++;
		if (index.GetColor() == DistrictCard::Color::Green)
			distinctDistrictColors++;
		if (index.GetColor() == DistrictCard::Color::Purple)
			distinctDistrictColors++;
		if (index.GetColor() == DistrictCard::Color::Red)
			distinctDistrictColors++;
		if (index.GetColor() == DistrictCard::Color::Yellow)
			distinctDistrictColors++;
		if (distinctDistrictColors == 5)
			return true;
	}
	return false;
}

void DistrictDeck::eliminateFirstCardFromDeck()
{
	deckCards.erase(deckCards.begin());
}

void DistrictDeck::eliminateSecondCardFromDeck()
{
	deckCards.erase(deckCards.begin()+1);
}

void DistrictDeck::sendCardToTheBackOfTheDeck(std::vector<DistrictCard>::iterator cardToBeSent)
{
	std::iter_swap(cardToBeSent, deckCards.end()-1);
}

void DistrictDeck::sendCardToTheBackOfTheDeckMagician(DistrictCard cardToBeSent)
{
	deckCards.push_back(cardToBeSent);
}

unsigned DistrictDeck::goldFromNobleDistricts()
{
	unsigned ammount = 0;
	for (auto card : deckCards)
		if (card.GetColor() == DistrictCard::Color::Yellow)
			ammount += 1;
	return ammount;
}

unsigned DistrictDeck::goldFromReligiousDistricts()
{
	unsigned ammount = 0;
	for (auto card : deckCards)
		if (card.GetColor() == DistrictCard::Color::Blue)
			ammount += 1;
	return ammount;
}

unsigned DistrictDeck::goldFromTradeDistricts()
{
	unsigned ammount = 0;
	for (auto card : deckCards)
		if (card.GetColor() == DistrictCard::Color::Green)
			ammount += 1;
	return ammount;
}

unsigned DistrictDeck::goldFromMilitaryDistricts()
{
	unsigned ammount = 0;
	for (auto card : deckCards)
		if (card.GetColor() == DistrictCard::Color::Red)
			ammount += 1;
	return ammount;
}

std::vector<DistrictCard>::iterator DistrictDeck::firstCardInDeck()
{
	//std::cout << "test: " << (*deckCards.begin()).getName() << "\n";
	return deckCards.begin();
}

std::vector<DistrictCard>::iterator DistrictDeck::secondCardInDeck()
{
	return deckCards.begin() + 1;
}

std::vector<DistrictCard>::iterator DistrictDeck::lastCardInDeck()
{
	return deckCards.end() - 1;
}





void DistrictDeck::setNumberOfPlayers(unsigned nrOfPlayers)
{
	numberOfPlayers = nrOfPlayers;
}

# Citadels

Proiect Citadels, simulare joc creata in C++
Modern C++, grupa 10LF272

Membri:
Mihai Alexandru Teodor
Manea Robert Constantin
Maner Danut Sebastian

Reguli oficiale Citadels: https://www.fgbradleys.com/rules/rules2/Citadels-rules.pdf

NOTE:
1* Clasa Deck a fost abandonata pe parcursul implementarii; ea nu va fi utilizata in versiunea finala a proiectului
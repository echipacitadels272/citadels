#pragma once
#include"Card.h"
#include"CharactherCard.h"
#include"DistrictCard.h"
#include<vector>
#include<algorithm>
#include<iostream>
//#include<iterator>

const unsigned int kInvalidNumberOfPlayers = 0;

class Deck
{
public:
	Deck();
	Deck(int nrOfPlayers);
	Deck(const Deck& otherDeck);
	Deck(Deck&& otherDeck);

public:
	Deck& operator = (const Deck& otherDeck);
	Deck& operator = (Deck&& otherDeck);

public:
	void generateMixedDeck();
	void generateCharactherDeck();
	void generateDistrictDeck();
	void Shuffle();
	void printCards();

protected:
	std::vector<Card> deckCards;
	unsigned numberOfPlayers;
};



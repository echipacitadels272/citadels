#include "Card.h"
#include<iostream>

std::string Card::getName() const
{
	return card_name;
}

void Card::setName(std::string name)
{
	card_name = name;
}

bool Card::getFacingDown() const
{
	return isFacingDown;
}

void Card::flipCard()
{
	isFacingDown = !isFacingDown;
}

Card::Card()
{
	card_name = kInvalidCardName;
	isFacingDown = true;
}

Card::Card(const std::string & name)
{
	card_name = name;
	isFacingDown = true;
}

Card::Card(const Card & other_card)
{
	*this = other_card;
}

Card::Card(Card && other_card)
{
	*this = std::move(other_card);
}


Card::~Card()
{
	card_name = kInvalidCardName;
	isFacingDown = true;
}

void Card::setfacing(bool cardOrientation)
{
	isFacingDown = cardOrientation;
}

std::string Card::getName()
{
	return card_name;
}

Card & Card::operator=(const Card & other_card)
{
	card_name = other_card.card_name;
	return *this;
}

Card & Card::operator=(Card && other_card)
{
	card_name = std::move(other_card.card_name);
	isFacingDown = std::move(other_card.isFacingDown);
	new(&other_card) Card;
	return *this;
}

std::ostream & operator<<(std::ostream & output, const Card & card)
{
	return output << card.card_name << std::endl;
}

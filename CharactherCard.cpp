#include "CharactherCard.h"
#include<iostream>


CharactherCard::CharactherCard():
	Card()
{
	CharactherCard(kInvalidIndex, kInvalidName, Type::None);
}

CharactherCard::CharactherCard(unsigned int index, std::string name, Type type):
	Card(name)
{
	m_index = index;
	m_type = type;
}

CharactherCard::CharactherCard(const CharactherCard & otherCard)
{
	*this = otherCard;
}


/*
CharactherCard::CharactherCard(CharactherCard && otherCard)
{
	*this = otherCard;
}
*/

CharactherCard::~CharactherCard()
{
	m_index = -1;
	m_type = CharactherCard::Type::None;
}

CharactherCard::Type CharactherCard::getType() const
{
	return m_type;
}

unsigned CharactherCard::getIndex() const
{
	return m_index;
}

std::string CharactherCard::getCharacterCardName() const
{
	std::string name = this->getName();
	return name;
}

CharactherCard & CharactherCard::operator=(const CharactherCard &otherCard)
{
	m_index = otherCard.m_index;
	m_type = otherCard.m_type;
	std::string name = otherCard.getCharacterCardName();
	this->setName(name);

	return *this;
}
/*
CharactherCard & CharactherCard::operator=(CharactherCard && otherCard)
{
	m_index = std::move(otherCard.m_index);
	m_type = std::move(otherCard.m_type);
	card_name = std::move(otherCard.card_name);		

	new(&otherCard) CharactherCard;

	return *this;
}*/

std::ostream & operator<<(std::ostream & output, CharactherCard & charactherCard)
{
	std::cout << "ch. card index: " << charactherCard.m_index <<
		"\nch. card name: " << charactherCard.getName() << "\nch. card type: ";

	if (charactherCard.m_type == CharactherCard::Type::Architect)
		std::cout << "architect" << std::endl;

	if (charactherCard.m_type == CharactherCard::Type::Thief)
		std::cout << "thief" << std::endl;

	if (charactherCard.m_type == CharactherCard::Type::Assassin)
		std::cout << "assassin" << std::endl;

	if (charactherCard.m_type == CharactherCard::Type::Bishop)
		std::cout << "bishop" << std::endl;

	if (charactherCard.m_type == CharactherCard::Type::King)
		std::cout << "king" << std::endl;

	if (charactherCard.m_type == CharactherCard::Type::Magician)
		std::cout << "magician" << std::endl;

	if (charactherCard.m_type == CharactherCard::Type::Merchant)
		std::cout << "merchant" << std::endl;

	if (charactherCard.m_type == CharactherCard::Type::Warlord)
		std::cout << "warlord" << std::endl;

	return output;
}

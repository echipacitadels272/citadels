#include "Player.h"


//CONSTRUCTORS
Player::Player():
	m_name(kInvalidPlayerName),
	m_age(KInvalidAge),
	m_numberOfGold(KInvalidAmountOfGold),
	m_numberOfPoints(KInvalidNumberOfPoints)
{
}

Player::Player(const std::string & name, const unsigned int & age, const unsigned int & goldAmount) :
	m_name(name),
	m_age(age),
	m_numberOfGold(goldAmount)
{
}

Player::Player(const Player & otherPlayer)
{
	*this = otherPlayer;
}

Player::Player(Player && otherPlayer)
{
	*this = std::move(otherPlayer);
}


Player::~Player()
{
	this->m_name = kInvalidPlayerName;
	this->m_age = KInvalidAge;
	this->m_numberOfGold = KInvalidAmountOfGold;
	this->m_numberOfPoints = KInvalidNumberOfPoints;
}



//SETTERS
void Player::setName(const std::string & name)
{
	this->m_name = name;
}

void Player::setAge(unsigned int & age)
{
	if (age < 7)
		throw "The player doesn't have the suitable age to play this game. \n";
	this->m_age = age;
}

void Player::setNumberOfGold(unsigned int & goldAmount)
{
	if (goldAmount == 0)
		throw "A player cannot receive such a number of gold. \n";
	this->m_numberOfGold = goldAmount;
}

void Player::setCrown()
{
	m_hasCrown = true;
}

void Player::resetCrown()
{
	m_hasCrown = false;
}

void Player::setCharacterCard(CharactherCard &card)
{
	CharactherCardOwned = card;
}


void Player::setindexOfCharacterCardOwned(unsigned & index)
{
	indexOfCharacterCardOwned = index;
}

void Player::setDistrictCard(const DistrictCard & card)
{
	districtCard = card;
}



//GETTERS
std::string Player::getName() const
{
	return this->m_name;
}

unsigned int Player::getAge() const
{
	return this->m_age;
}

unsigned int Player::getNumberOfGold() const
{
	return this->m_numberOfGold;
}

CharactherCard Player::getCharactherCard() const
{
	return CharactherCardOwned;
}

unsigned Player::getCharactherCardIndex() const
{
	return indexOfCharacterCardOwned;
}


unsigned int Player::getNumberOfPoints() const
{
	return this->m_numberOfPoints;
}

unsigned Player::getGoldValueOfCitadel()
{
	return city.returnGoldValueOfDeck();
}

bool Player::deckOfFiveDistinctColors()
{
	return city.containsFiveDistrictColors();
}

unsigned Player::ammountOfGold()
{
	return m_numberOfGold;
}


// OPERATORS
Player & Player::operator=(const Player & otherPlayer)
{
	this->m_name = otherPlayer.m_name;
	this->m_age = otherPlayer.m_age;
	this->m_numberOfGold = otherPlayer.getNumberOfGold();

	return *this;
}

Player & Player::operator=(Player && otherPlayer)
{
	this->m_name = otherPlayer.m_name;
	this->m_age = otherPlayer.m_age;
	this->m_numberOfGold = otherPlayer.getNumberOfGold();

	new(&otherPlayer) Player;
	return *this;
}

bool Player::operator==(const Player & otherPlayer)
{
	if (m_name == otherPlayer.getName())
		return true;
	return false;
}



// RELEVANT FUNCTIONS FOR THE GAME
bool Player::hasCrown() const
{
	return m_hasCrown;
}

bool Player::hasWinningState() const
{
	if (city.getDeckSize() == KDistrictCardsToWin)
		return true;

	return false;
}



void Player::addDistrictCardToPlayerCity(DistrictCard &card)
{
	city.insertDistrictCard(card);
}


void Player::getGoldFromPlayer(unsigned int amount)
{
	if (amount > this->m_numberOfGold)
		throw "The amount of gold of this player is less than the amount you want to take from. \n";
	this->m_numberOfGold -= amount;
}

void Player::giveGoldToPlayer(unsigned int amount)
{
	this->m_numberOfGold += amount;
}
void Player::setNumberOfPoints(unsigned points)
{
	m_numberOfPoints = points;
}

unsigned Player::giveAllGold_This_Is_a_Robbery()
{
	unsigned allTheGold = m_numberOfGold;
	m_numberOfGold = 0;
	return allTheGold;
}

unsigned Player::getCityDeckSize()
{
	return city.getDeckSize();
}

DistrictCard Player::removeDistrictCardFromCity()
{
	DistrictCard cardToBeSent = city.getCard();
	return cardToBeSent;
}

DistrictDeck Player::sendDeck()
{
	return city;
}

void Player::setDeck(DistrictDeck receivedDeck)
{
	city = receivedDeck;
}

void Player::exchangeCities(Player & otherPlayer)
{
	DistrictDeck bufferDeck = this->city;
	this->city = otherPlayer.sendDeck();
	otherPlayer.setDeck(bufferDeck);
}

void Player::kingGetsGoldFromNobleDistricts()
{
	m_numberOfGold += city.goldFromNobleDistricts();
}

void Player::bishopGetsGoldFromReligiousDistricts()
{
	m_numberOfGold += city.goldFromReligiousDistricts();
}

void Player::merchantGetsGoldFromTradeDistricts()
{
	m_numberOfGold += city.goldFromTradeDistricts();
}

void Player::warlordGetsGoldFromMilitaryDistricts()
{
	m_numberOfGold += city.goldFromMilitaryDistricts();
}


// PRINTING OPERATOR
std::ostream & operator<<(std::ostream & output, const Player & player)
{
	output << "Player " << player.m_name << " has " << player.m_age << " years " << " and " << player.m_numberOfGold << " gold." << std::endl;
	return output;
}

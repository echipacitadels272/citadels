#pragma once
#include<string>
#include<ctime>		//va fi folosit in deck-uri

const std::string kInvalidCardName = "#";

class Card
{
public:

	std::string getName() const;
	void setName(std::string name);
	
	bool getFacingDown() const;
	void flipCard();

	Card();
	Card(const std::string & name);
	Card(const Card & other_card);
	Card(Card && other_card);
	virtual ~Card();
	void setfacing(bool cardOrientation);
	std::string getName();

	friend std::ostream & operator << (std::ostream & output, const Card & card);
	Card & operator = (const Card & other_card);
	Card & operator = (Card && other_card);

protected:
	std::string card_name;
	bool isFacingDown;
};


#pragma once
#include<iostream>
#include<string>
#include<vector>
#include"CharactherCard.h"
#include"DistrictDeck.h"

const std::string kInvalidPlayerName = "^";


class Player
{
private:
	
	//static variables
	static const unsigned int KInvalidAge = 0;
	static const unsigned int KInvalidAmountOfGold = 0;
	static const unsigned int KInvalidNumberOfPoints=0;
	static const unsigned int KDistrictCardsToWin = 8;
	
	std::string m_name;
	unsigned int m_age;
	unsigned int m_numberOfGold;
	unsigned int m_numberOfPoints;
	bool m_hasCrown = false;
	
	DistrictCard districtCard;
	//DistrictDeck districtCardsInHand;
	DistrictDeck city;
	CharactherCard CharactherCardOwned;
	unsigned indexOfCharacterCardOwned;
public:
	
	// constructors
	Player();
	Player(const std::string & name, const unsigned int & age, const unsigned int & goldAmount);
	Player(const Player & otherPlayer);
	Player( Player && otherPlayer);

	~Player();

	//setters
	void setName(const std::string & name);
	void setAge(unsigned int & age);
	void setNumberOfGold(unsigned int & goldAmount);
	void setCrown();
	void resetCrown();
	void setCharacterCard(CharactherCard &card);
	void setindexOfCharacterCardOwned(unsigned &index);
	void setDistrictCard(const DistrictCard & card);

	//getters

	std::string getName() const;
	unsigned int getAge() const;
	unsigned int getNumberOfGold() const;
	CharactherCard getCharactherCard() const;
	unsigned getCharactherCardIndex() const;
	//DistrictCard getDistrictCard() const;
	unsigned int getNumberOfPoints() const;
	unsigned getGoldValueOfCitadel();
	bool deckOfFiveDistinctColors();
	unsigned ammountOfGold();

	// operators  overloading

	friend std::ostream& operator <<(std::ostream & output, const Player & player);

	Player & operator = (const Player& otherPlayer);
	Player & operator = (Player&& otherPlayer);
	bool operator == (const Player& otherPlayer);

	// functions

	bool hasCrown() const;
	bool hasWinningState() const;
	void addDistrictCardToPlayerCity(DistrictCard &card);
	void getGoldFromPlayer(unsigned int amount);
	void giveGoldToPlayer(unsigned int amount);
	void setNumberOfPoints(unsigned points);
	unsigned giveAllGold_This_Is_a_Robbery();
	unsigned getCityDeckSize();
	DistrictCard removeDistrictCardFromCity();
	DistrictDeck sendDeck();
	void setDeck(DistrictDeck receivedDeck);
	void exchangeCities(Player &otherPlayer);
	void kingGetsGoldFromNobleDistricts();
	void bishopGetsGoldFromReligiousDistricts();
	void merchantGetsGoldFromTradeDistricts();
	void warlordGetsGoldFromMilitaryDistricts();
};


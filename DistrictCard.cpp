#include<iostream>

#include "DistrictCard.h"

const int kInvalidCostInGold=-1;
const std::string kInvalidName = "#";


DistrictCard::DistrictCard():
	DistrictCard(kInvalidCostInGold,kInvalidName,Color::None)
{
}

DistrictCard::DistrictCard(unsigned int cost, std::string name, Color color):
Card(name)
{
	m_costInGold = cost;
	m_color = color;
}

DistrictCard::DistrictCard(const DistrictCard & other_card)
{
	*this=other_card;
}

DistrictCard::DistrictCard(DistrictCard && other_card)
{
	*this = std::move(other_card);
}

unsigned int DistrictCard::GetCostInGold() const
{
	return m_costInGold;
}

std::string DistrictCard::GetDistrictCardName() const
{
	return this->getName();
}

DistrictCard::Color DistrictCard::GetColor() const
{
	return m_color;
}

DistrictCard & DistrictCard::operator = (const DistrictCard & other_card)
{
	m_costInGold = other_card.m_costInGold;
	std::string name = other_card.GetDistrictCardName();
	this->setName(name);
	m_color = other_card.m_color;

	return *this;
}

DistrictCard & DistrictCard::operator = (DistrictCard && other_card)
{
	std::string name = other_card.GetDistrictCardName();
	this->setName(name);
	m_costInGold = other_card.m_costInGold;
	m_color = other_card.m_color;

	new(&other_card)  DistrictCard;

	return *this;
}

DistrictCard::~DistrictCard()
{
	m_costInGold = kInvalidCostInGold;
	this->setName(kInvalidName);
	m_color = Color::None;
}

std::ostream & operator<<(std::ostream & output, DistrictCard & other_card)
{
	return output <<
		other_card.GetDistrictCardName() << std::endl <<
		other_card.m_costInGold << std::endl <<
		static_cast<int>(other_card.m_color);
}

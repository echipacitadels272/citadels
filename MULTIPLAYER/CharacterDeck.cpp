#include "CharacterDeck.h"

CharacterDeck::CharacterDeck()
{
}


CharacterDeck::CharacterDeck(const CharacterDeck & otherDeck)
{
	*this = otherDeck;
}

CharacterDeck::CharacterDeck(CharacterDeck && otherDeck)
{
	*this = std::move(otherDeck);
}

CharacterDeck & CharacterDeck::operator=(const CharacterDeck & otherDeck)
{
	deckCards = otherDeck.deckCards;
	return *this;
}

CharacterDeck & CharacterDeck::operator=(CharacterDeck && otherDeck)
{
	deckCards = std::move(otherDeck.deckCards);

	new(&otherDeck) CharacterDeck;

	return *this;
}

void CharacterDeck::generateCharactherDeck()
{
	std::string names[] = { "Assassin", "Thief", "Magician", "King", "Bishop", "Merchant", "Architect", "Warlord" };
	/*
	for (uint8_t t = 1U; t <= 8U; ++t)
	{
		for (unsigned i = 0; i < numberOfPlayers; ++i)
		{
			CharactherCard currentCard(t, names[t - 1], static_cast<CharactherCard::Type>(t));	
			deckCards.push_back(currentCard);

		}
	}
	*/

	for (unsigned t = 1; t <= 8; ++t)
	{
			CharactherCard::Type cardType;
			if (t == 1)
				cardType = CharactherCard::Type::Assassin;
			if (t == 2)
				cardType = CharactherCard::Type::Thief;
			if (t == 3)
				cardType = CharactherCard::Type::Magician;
			if (t == 4)
				cardType = CharactherCard::Type::King;
			if (t == 5)
				cardType = CharactherCard::Type::Bishop;
			if (t == 6)
				cardType = CharactherCard::Type::Merchant;
			if (t == 7)
				cardType = CharactherCard::Type::Architect;
			if (t == 8)
				cardType = CharactherCard::Type::Warlord;
			CharactherCard currentCard(t, names[t - 1], cardType);
			deckCards.push_back(currentCard);

	}
}



void CharacterDeck::Shuffle()
{
	srand(time(0));
	std::random_shuffle(deckCards.begin(), deckCards.end());
}

void CharacterDeck::printCards()
{
	for (unsigned i = 0; i < deckCards.size(); ++i)
		if (!deckCards[i].getFacingDown())
			std::cout << "*facing down card* ";
		else
			std::cout <<"for "<< deckCards[i].getName() << " choose "<<deckCards[i].getIndex()<<std::endl;
	std::cout << std::endl;
}

void CharacterDeck::printBurntCards()
{
	std::cout << "Burnt cards this round: ";
	for (unsigned i = 0; i < deckCards.size(); ++i)
		if (!deckCards[i].getFacingDown())
			std::cout << "*facing down card*, ";
		else
			std::cout <<deckCards[i].getName() << ", ";
	std::cout << std::endl;

}

CharacterDeck & CharacterDeck::getDeck()
{
	return *this;
}

CharactherCard & CharacterDeck::getCard()	
{
	return deckCards.front();
}

unsigned CharacterDeck::getDeckSize() const
{
	return deckCards.size();
}

void CharacterDeck::getCertainCard(unsigned chIndex, CharactherCard& emptyCard)
{
	for (auto card : deckCards)
		if (card.getIndex() == chIndex)
		{
			emptyCard = card;
			break;
		}
}

void CharacterDeck::eliminateFirstCardFromDeck()
{
	deckCards.erase(deckCards.begin());
}

void CharacterDeck::eliminateCertainCardFromDeck(unsigned index)
{
	std::vector<CharactherCard>::iterator cardToBeDeleted;
	for(cardToBeDeleted = deckCards.begin(); cardToBeDeleted<deckCards.end(); cardToBeDeleted++)
		if((*cardToBeDeleted).getIndex()==index){
			deckCards.erase(cardToBeDeleted);
			return;
		}

}

unsigned CharacterDeck::returnIndexOfCardInDeck(CharactherCard & card)
{
	for (unsigned index = 0; index < deckCards.size(); ++index)
		if (deckCards[index].getIndex() == card.getIndex())
			return index;

}

std::vector<CharactherCard>::iterator CharacterDeck::firstCardInDeck()
{
	return deckCards.begin();
}

std::vector<CharactherCard>::iterator CharacterDeck::lastCardInDeck()
{
	return deckCards.end() - 1;
}


void CharacterDeck::addCard(const CharactherCard & card)
{
	bool cardOrientation = card.getFacingDown();
	deckCards.push_back(card);
	deckCards.front().setfacing(cardOrientation);
}

void CharacterDeck::resetDeck()
{
	deckCards.clear();
}

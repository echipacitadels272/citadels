#include "Game.h"
#include<stdlib.h>
#include<cctype>
#include<sstream>

/*
AM ABANDONAT CLASA DECK DATORITA AMBIGUITATII TIPULUI CARD GENERATA DE INTERPRETAREA GRESITA A POLIMORFISMULUI
*/

unsigned kInvalidPlayerIndex = 0;

void Game::initGame()
{
	system("cls");
	//SETTING UP THE TABLE
	goldInBank = 500000;	//in Citadels, the amount of gold in the bank is unlimited
	bool gameHasEnded = false;

	while (true) {
		std::cout << "How many players would like to join?" << std::endl;
		std::cout << "(please note that Citadels is usually a game of 4 players, but 5 players can join too!)" << std::endl;
		std::string userInput;
		unsigned userInputValue;
		std::cin >> userInput;
		std::istringstream s(userInput);
		s >> userInputValue;

		if ((userInputValue != 4) && (userInputValue != 5))
		{
			std::cout << "Invalid option!" << std::endl;
		}
		else
		{
			numberOfPlayers = userInputValue;
			break;
		}
	}

	gameBoard.setNumberOfPlayers(numberOfPlayers);

	for (unsigned i = 0; i < numberOfPlayers; ++i) {
		system("cls");
		std::string  name;
		unsigned age;
		std::cout << "insert player name and age for player #" << i + 1 << std::endl;
		std::cin >> name >> age;
		Player newPlayer(name, age, 2);
		goldInBank--;
		gameBoard.addPlayer(newPlayer);
		system("cls");
	}

	gameBoard.initialiseDecks(numberOfPlayers);

	for (unsigned i = 0; i < numberOfPlayers; ++i) {

		for (unsigned j = 0; j < KInitialNumberOfDisitrictCards; ++j) {
			gameBoard.sendDistrictCardToPlayer(i);
		}
	}

	gameBoard.giveCrown(gameBoard.oldestPlayer());

	//PLAYING THE GAME

	CharactherCard burntFaceDownCard;
	CharactherCard cardLeftInDeck;
	CharactherCard burntFaceUpCard1;
	CharactherCard burntFaceUpCard2;
	CharacterDeck burntCards;


	while (!gameHasEnded) {
		if (!goldInBank)
			goldInBank = 500000;
		//GAMELOOP, 1 LOOP/TURA

		burntCards.resetDeck();	//so the burnt cards don't keep each round

		//STEP #1
		burntFaceDownCard = gameBoard.getCharacterCard();
		if (burntFaceDownCard.getFacingDown() == false)
			burntFaceDownCard.flipCard();
		gameBoard.eliminateFirstCardFromCharacterDeck();
		burntCards.addCard(burntFaceDownCard);

		if (numberOfPlayers == 4)
		{
			burntFaceUpCard1 = gameBoard.getCharacterCard();
			gameBoard.eliminateFirstCardFromCharacterDeck();
			burntFaceUpCard2 = gameBoard.getCharacterCard();
			gameBoard.eliminateFirstCardFromCharacterDeck();
			burntFaceUpCard1.flipCard();
			burntFaceUpCard2.flipCard();
			burntCards.addCard(burntFaceUpCard1);
			burntCards.addCard(burntFaceUpCard2);
		}

		if (numberOfPlayers == 5) {
			burntFaceUpCard1 = gameBoard.getCharacterCard();
			gameBoard.eliminateFirstCardFromCharacterDeck();
			burntFaceUpCard1.flipCard();
			burntCards.addCard(burntFaceUpCard1);
		}

		burntCards.printBurntCards();

		//STEP #2
		std::vector<Player>::iterator crownPlayer = gameBoard.playerWithCrown();
		std::vector<Player>::iterator currentPlayer = crownPlayer + 1;

		unsigned selectedIndex;

		if (crownPlayer == gameBoard.lastPlayerInVector())
			currentPlayer = gameBoard.firstPlayerInVector();

		std::cout << std::endl << (*crownPlayer).getName() << " , you are the crown player, choose your character card from these available:" << std::endl;
		gameBoard.printCharactersDeck();

		while (true) {
			gameHasEnded = false;
			std::cin >> selectedIndex;
			if (selectedIndex < 1 || selectedIndex > 8)
			{
				std::cout << "Burnt or invalid card" << std::endl;
			}
			else
			{
				bool ok = true;
				for (std::vector<CharactherCard>::iterator burnt = burntCards.firstCardInDeck(); burnt <= burntCards.lastCardInDeck(); burnt++)
					if ((*burnt).getIndex() == selectedIndex)
					{
						ok = false;
						std::cout << "Burnt or invalid card" << std::endl;
					}
				if (ok)
					break;
			}
		}
		gameBoard.sendCharactherCardToPlayer(gameBoard.getPlayerIndex((*crownPlayer)), selectedIndex);
		burntCards.addCard((*crownPlayer).getCharactherCard());
		gameBoard.eliminateCharacterCardByCharacterIndex(selectedIndex);
		system("cls");

		while (currentPlayer != crownPlayer) {
			burntCards.printBurntCards();
			std::cout << (*currentPlayer).getName() << " ,choose your character card from these available:" << std::endl;
			gameBoard.printCharactersDeck();
			while (true) {
				std::cin >> selectedIndex;
				if (selectedIndex < 1 || selectedIndex > 8)
				{
					std::cout << "Burnt or invalid card" << std::endl;
				}
				else
				{
					bool ok = true;
					for (std::vector<CharactherCard>::iterator burnt = burntCards.firstCardInDeck(); burnt <= burntCards.lastCardInDeck(); burnt++)
						if ((*burnt).getIndex() == selectedIndex)
						{
							ok = false;
							std::cout << "Burnt or invalid card" << std::endl;
						}
					if (ok)
						break;
				}
			}
			gameBoard.sendCharactherCardToPlayer(gameBoard.getPlayerIndex((*currentPlayer)), selectedIndex);
			burntCards.addCard((*currentPlayer).getCharactherCard());
			gameBoard.eliminateCharacterCardByCharacterIndex(selectedIndex);

			if (currentPlayer == gameBoard.lastPlayerInVector())
				currentPlayer = gameBoard.firstPlayerInVector();
			else
				currentPlayer = currentPlayer + 1;

			system("cls");
		}

		if (numberOfPlayers == 4 || numberOfPlayers == 5) {
			cardLeftInDeck = gameBoard.getCharacterCard();
			gameBoard.eliminateFirstCardFromCharacterDeck();
			burntCards.addCard(cardLeftInDeck);
		}
		std::cout << std::endl << "All character cards have been either picked or 'burned' !" << std::endl;

		//STEP #3

		unsigned murderedCharacterIndex = kInvalidPlayerIndex;
		unsigned muggedCharacterIndex = kInvalidPlayerIndex;
		std::vector<Player>::iterator thiefPlayer;
		std::vector<Player>::iterator magicianPlayer;
		std::vector<Player>::iterator warlordPlayer;
		std::string playerNameMagicianSwap = kInvalidPlayerName;
		unsigned magicianOption = 0;
		std::string districtDestroyedByWarlord;
		unsigned thiefUserIndex = kInvalidPlayerIndex;

		for (unsigned indexCard = 1; indexCard <= 8; ++indexCard) {

			for (currentPlayer = gameBoard.firstPlayerInVector(); currentPlayer <= gameBoard.lastPlayerInVector(); currentPlayer++)
				if ((*currentPlayer).getCharactherCardIndex() == indexCard && (*currentPlayer).getCharactherCardIndex() != murderedCharacterIndex) {

					//King gets gold from his districts
					if (currentPlayer == crownPlayer)
						(*currentPlayer).kingGetsGoldFromNobleDistricts();

					//Bishop gets gold from his districts
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Bishop)
						(*currentPlayer).bishopGetsGoldFromReligiousDistricts();

					//Merchant gets gold from his districts
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Merchant)
						(*currentPlayer).merchantGetsGoldFromTradeDistricts();

					//Warlord gets gold from his districts
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Warlord)
						(*currentPlayer).warlordGetsGoldFromMilitaryDistricts();

					std::cout << std::endl;
					std::cout << "It is " << (*currentPlayer).getName() << "'s turn!" << std::endl;
					if (currentPlayer == crownPlayer)
						std::cout << (*currentPlayer).getName() << " is the crown player!" << std::endl;

					//if you got targeted by the thief
					if ((*currentPlayer).getCharactherCardIndex() == muggedCharacterIndex && (*currentPlayer).getCharactherCard().getType() != CharactherCard::Type::Assassin && (*currentPlayer).getCharactherCardIndex() != murderedCharacterIndex)
					{
						std::cout << "Oh, no! The thief just stole all your gold!" << std::endl;
						(*thiefPlayer).giveGoldToPlayer((*currentPlayer).giveAllGold_This_Is_a_Robbery());
					}
					//

					//If you got targeted by the magician
					if (magicianOption == 1 && (*currentPlayer).getName() == playerNameMagicianSwap)
					{
						std::cout << "Oh, no! The magician swapped your cards with " << (*magicianPlayer).getName() << "'s!" << std::endl;
						(*currentPlayer).exchangeCities((*magicianPlayer));
					}
					//

					std::cout << "His/Her character card is the " << (*currentPlayer).getCharactherCard().getName() << std::endl;
					std::cout << "He/She has " << (*currentPlayer).getNumberOfGold() << " gold" << std::endl;

					unsigned option;
					std::vector<DistrictCard>::iterator firstDrawnCard = gameBoard.firstDistrictCardInDeckIterator();
					std::vector<DistrictCard>::iterator secondDrawnCard = gameBoard.secondDistrictCardInDeckIterator();

					while (true) {
						bool ok = true;
						std::cout << std::endl << "Select 1 to draw 2 gold from the bank" << std::endl;
						std::cout << "Select 2 to two pick district cards from the District Deck,\nthen choose one card to put in your hand, and place the other card on the bottom of the District Deck" << std::endl;
						std::cin >> option;


						char buildOption;

						switch (option) {
						case 1:
							(*currentPlayer).giveGoldToPlayer(2);
							goldInBank -= 2;
							if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Merchant) {
								(*currentPlayer).giveGoldToPlayer(1);
								goldInBank -= 1;
							}
							break;
						case 2:
							std::cout << std::endl << "These are the 2 District cards that you drew:" << std::endl;
							std::cout << (*firstDrawnCard).GetDistrictCardName() << ", which has a cost of " << (*firstDrawnCard).GetCostInGold() << " gold, and " << (*secondDrawnCard).GetDistrictCardName() << " , which has a cost of " << (*secondDrawnCard).GetCostInGold() << " gold" << std::endl;
							std::cout << "Which card do you want to keep?\n" << "Select 1 for the first card\n" << "Select 2 for the second card" << std::endl;
							unsigned cardPicked;
							std::cin >> cardPicked;
							//TO DO: VEZI DACA ALT JUCATOR A LUAT CARTEA ASTA
							switch (cardPicked) {
							case 1:
								std::cout << "You picked the " << (*firstDrawnCard).GetDistrictCardName() << " card, which has a cost of " << (*firstDrawnCard).GetCostInGold() << " gold" << std::endl;
								if ((*firstDrawnCard).GetDistrictCardName() == districtDestroyedByWarlord)
								{
									std::cout << "Oh,no! The Warlord destroyed this district!" << std::endl;
									(*warlordPlayer).getGoldFromPlayer((*firstDrawnCard).GetCostInGold() - 1);
									goldInBank += (*firstDrawnCard).GetCostInGold() - 1;
									break;
								}
								else {
									std::cout << "You have " << (*currentPlayer).getNumberOfGold() << " gold. Do you wish to build this card and add it into your city? Type 'y' for yes, or 'n' for no" << std::endl;
									std::cin >> buildOption;
									if (buildOption == 'y' || buildOption == 'Y') {
										if ((*currentPlayer).getNumberOfGold() >= (*firstDrawnCard).GetCostInGold()) {
											gameBoard.sendCardToTheBackOfTheDistrictDeck(secondDrawnCard);
											(*currentPlayer).getGoldFromPlayer((*firstDrawnCard).GetCostInGold());
											(*currentPlayer).addDistrictCardToPlayerCity((*firstDrawnCard));
											gameBoard.removeFirstDistrictCardInDeck();
											std::cout << "You added this card to your city!" << std::endl;
											gameBoard.shuffleDistrictDeck();
										}
										else {
											std::cout << "Sorry, you do not have enough gold to build that card yet!" << std::endl;
										}
									}
									break;
								}
							case 2:
								std::cout << "You picked the " << (*secondDrawnCard).GetDistrictCardName() << " card, which has a cost of " << (*secondDrawnCard).GetCostInGold() << " gold" << std::endl;
								if ((*secondDrawnCard).GetDistrictCardName() == districtDestroyedByWarlord)
								{
									std::cout << "Oh,no! The Warlord destroyed this district!" << std::endl;
									(*warlordPlayer).getGoldFromPlayer((*secondDrawnCard).GetCostInGold() - 1);
									goldInBank += (*secondDrawnCard).GetCostInGold() - 1;
									break;
								}
								else {
									std::cout << "You have " << (*currentPlayer).getNumberOfGold() << " gold. Do you wish to build this card and add it into your city? Type 'y' for yes, or 'n' for no" << std::endl;
									std::cin >> buildOption;
									if (buildOption == 'y' || buildOption == 'Y') {
										if ((*currentPlayer).getNumberOfGold() >= (*secondDrawnCard).GetCostInGold()) {
											gameBoard.sendCardToTheBackOfTheDistrictDeck(firstDrawnCard);
											(*currentPlayer).getGoldFromPlayer((*secondDrawnCard).GetCostInGold());
											(*currentPlayer).addDistrictCardToPlayerCity((*secondDrawnCard));
											gameBoard.removeSecondDistrictCardInDeck();
											std::cout << "You added this card to your city!" << std::endl;
											gameBoard.shuffleDistrictDeck();
										}
										else {
											std::cout << "Sorry, you do not have enough gold to build that card yet!" << std::endl;
										}
									}
									break;
								}
							default:
								std::cout << "Invalid option!" << std::endl;
							}
							gameBoard.shuffleDistrictDeck();	//so the next player draws another cards
							break;	//pt case 2 al switch-ului alegerilor playerului
						default:
							std::cout << "Invalid option!" << std::endl;
							ok = false;
						}
						if (ok)
							break;
					}
					//system("cls");
					//STILL ON IF BRANCH WHERE PLAYER SELECTED BY CH CARD
					std::cout << "You may still use your character card's special power: " << std::endl;
					//ASSASSIN
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Assassin) {
						std::cout << "Announce the title of another character that you wish to murder. The player who has the murdered character must say nothing, and must remain silent when the murdered character is called upon to take his turn. The murdered character misses his entire turn." << std::endl;
					}
					//THIEF
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Thief) {
						std::cout << "Announce the title of a character from whom you wish to steal. When the player who has that character is called upon to take his turn, you first take all of his gold. You may not steal from the Assassin or the Assassin's target." << std::endl;
					}
					//MAGICIAN
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Magician) {
						std::cout << "At any time during your turn, you have one of two options:" << std::endl;
						std::cout << "1:Exchange your entire hand of cards (not the cards in your city) with the hand of another player (this applies even if you have no cards in your hand, in which case you simply take the other player's cards)." << std::endl;
						std::cout << "2:Place any number of cards from your hand facedown at the bottom of the District Deck, and then draw an equal number of cards from the top of the District Deck." << std::endl;
					}
					//KING
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::King) {
						std::cout << "You receive one gold for each noble (yellow) district in your city. When the King is called, you immediately receive the Crown.You now call the characters, and you will be the first player to choose your character during the next round.If there is no King during the next round, you keep the Crown.If you are murdered, you skip your turn like any other character.Nevertheless, after the last player has played his turn, when it becomes known that you had the murdered King's character card, you take the Crown(as the King's heir)." << std::endl;
					}
					//BISHOP
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Bishop) {
						std::cout << "You receive one gold for each religious (blue) district in your city. Your districts may not be destroyed/exchanged by the Warlord." << std::endl;
					}
					//MERCHANT
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Merchant) {
						std::cout << "You receive one gold for each trade (green) district in your city. After you take an action, you receive one additional gold." << std::endl;
					}
					//ARCHITECT
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Architect) {
						std::cout << "After you take an action, you draw two additional district cards and put both in your hand. You may build up to three districts during your turn." << std::endl;
					}
					//WARLORD
					if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Warlord) {
						std::cout << "You receive one gold for each military (red) district in your city. At the end of your turn, you may destroy one district of your choice by paying a number of gold equal to one less than the cost of the district. Thus, you may destroy a cost one district for free, a cost two district for one gold, or a cost six district for five gold, etc. You may destroy one of your own districts. You may not, however, destroy a district in a city that is already completed by having eight districts." << std::endl;
					}
					std::cout << "Type 'y' if you wish to use you character card's special ability" << std::endl;
					char optSpAbil;
					std::cin >> optSpAbil;
					system("cls");
					if (optSpAbil == 'y' || optSpAbil == 'Y') {
						if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Assassin) {
							std::cout << "Which character do you wish to 'murder?'";
							std::cout << "For Assassin, choose 1" << std::endl;
							std::cout << "For Thief, choose 2" << std::endl;
							std::cout << "For Magician, choose 3" << std::endl;
							std::cout << "For King, choose 4" << std::endl;
							std::cout << "For Bishop, choose 5" << std::endl;
							std::cout << "For Merchant, choose 6" << std::endl;
							std::cout << "For Architect, choose 7" << std::endl;
							std::cout << "For Warlord, choose 8" << std::endl;
							std::cin >> murderedCharacterIndex;
						}
						if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Thief) {
							thiefPlayer = currentPlayer;
							std::cout << "Which character do you wish to steal gold from?" << std::endl;
							std::cout << "For Assassin, choose 1" << std::endl;
							std::cout << "For Thief, choose 2" << std::endl;
							std::cout << "For Magician, choose 3" << std::endl;
							std::cout << "For King, choose 4" << std::endl;
							std::cout << "For Bishop, choose 5" << std::endl;
							std::cout << "For Merchant, choose 6" << std::endl;
							std::cout << "For Architect, choose 7" << std::endl;
							std::cout << "For Warlord, choose 8" << std::endl;
							std::cin >> muggedCharacterIndex;
						}
						if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Magician) {
							magicianPlayer = currentPlayer;
							std::cout << "Choose one of the two options:" << std::endl;
							std::cout << "1:Exchange your entire hand of cards (not the cards in your city) with the hand of another player (this applies even if you have no cards in your hand, in which case you simply take the other player's cards)." << std::endl;
							std::cout << "2:Place any number of cards from your hand facedown at the bottom of the District Deck, and then draw an equal number of cards from the top of the District Deck." << std::endl;
							unsigned magicianOption;
							DistrictCard bufferCard;
							unsigned numberOfCardsToExchangeWithBoard;
							std::cin >> magicianOption;
							switch (magicianOption) {
							case 1:
								std::cout << "Type the name of the player with whom you want to switch cards with" << std::endl;
								std::cin >> playerNameMagicianSwap;
								magicianOption = 1;
								break;
							case 2:
								std::cout << "How many district cards you wish to exchange with the deck on the table?" << std::endl;
								std::cout << "(Please note that the number must be at most equal with the number of district cards that you own, which is " << (*currentPlayer).getCityDeckSize() << std::endl;
								std::cin >> numberOfCardsToExchangeWithBoard;
								for (unsigned returnedCard = 0; returnedCard < numberOfCardsToExchangeWithBoard; ++returnedCard)
									gameBoard.sendCardToTheBackOfTheDistrictDeckMagician((*currentPlayer).removeDistrictCardFromCity());
								for (unsigned returnedCard = 0; returnedCard < numberOfCardsToExchangeWithBoard; ++returnedCard)
								{
									bufferCard = gameBoard.getDistrictCard();
									(*currentPlayer).addDistrictCardToPlayerCity(bufferCard);
								}
								break;
							default:
								std::cout << "Invalid option!" << std::endl;
							}
						}
						if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::King) {
							std::cout << "You are now the crown player!";
							(*crownPlayer).resetCrown();
							(*currentPlayer).setCrown();
							crownPlayer = currentPlayer;
						}
						if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Bishop) {
							/*You receive one gold for each religious (blue) district in your city. (implemented above)
							Your districts may not be destroyed/exchanged by the Warlord/*/
						}
						if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Merchant) {
							(*currentPlayer).giveGoldToPlayer(1);
							goldInBank -= 1;
							/*You receive one gold for each trade (green) district in your city.(implemented above.
							After you take an action, you receive one additional gold.
							*/
						}
						if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Architect) {
							std::vector<DistrictCard>::iterator firstDrawnCard = gameBoard.firstDistrictCardInDeckIterator();
							std::vector<DistrictCard>::iterator secondDrawnCard = gameBoard.secondDistrictCardInDeckIterator();

							char buildOption;
							std::cout << "You picked the " << (*firstDrawnCard).GetDistrictCardName() << " card, which has a cost of " << (*firstDrawnCard).GetCostInGold() << " gold" << std::endl;
							std::cout << "You have " << (*currentPlayer).getNumberOfGold() << " gold. Do you wish to build this card and add it into your city? Type 'y' for yes, or 'n' for no" << std::endl;
							std::cin >> buildOption;
							if (buildOption == 'y' || buildOption == 'Y') {
								if ((*currentPlayer).getNumberOfGold() >= (*firstDrawnCard).GetCostInGold()) {
									gameBoard.sendCardToTheBackOfTheDistrictDeck(secondDrawnCard);
									(*currentPlayer).getGoldFromPlayer((*firstDrawnCard).GetCostInGold());
									(*currentPlayer).addDistrictCardToPlayerCity((*firstDrawnCard));
									gameBoard.removeFirstDistrictCardInDeck();
									std::cout << "You added this card to your city!" << std::endl;
									gameBoard.shuffleDistrictDeck();
								}
								else {
									std::cout << "Sorry, you do not have enough gold to build that card yet!" << std::endl;
								}
							}

							std::cout << "You picked the " << (*secondDrawnCard).GetDistrictCardName() << " card, which has a cost of " << (*firstDrawnCard).GetCostInGold() << " gold" << std::endl;
							std::cout << "You have " << (*currentPlayer).getNumberOfGold() << " gold. Do you wish to build this card and add it into your city? Type 'y' for yes, or 'n' for no" << std::endl;
							std::cin >> buildOption;
							if (buildOption == 'y' || buildOption == 'Y') {
								if ((*currentPlayer).getNumberOfGold() >= (*secondDrawnCard).GetCostInGold()) {
									gameBoard.sendCardToTheBackOfTheDistrictDeck(firstDrawnCard);
									(*currentPlayer).getGoldFromPlayer((*secondDrawnCard).GetCostInGold());
									(*currentPlayer).addDistrictCardToPlayerCity((*secondDrawnCard));
									gameBoard.removeSecondDistrictCardInDeck();
									std::cout << "You added this card to your city!" << std::endl;
									gameBoard.shuffleDistrictDeck();
								}
								else {
									std::cout << "Sorry, you do not have enough gold to build that card yet!" << std::endl;
								}
							}

						}
						if ((*currentPlayer).getCharactherCard().getType() == CharactherCard::Type::Warlord) {
							std::cout << "Choose a district to destroy:" << std::endl;
							unsigned opt;
							std::string names[] = { "Manor", "Castle", "Palace", "Temple", "Church", "Monastery", "Cathedral", "Tavern" ,"Market", "Tranding Post", "Docks", "Harbor", "Town Hall",	"Watchtower", "Prison", "Battlefield", "Fortress", "Haunted City", "Keep", "Laboratory", "Smithy", "Graveyard", "Observatory", "School of Magic", "Library", "Great Wall", "University", "Dragon Gate" };
							for (unsigned index = 0; index <= 27; ++index) {
								std::cout << "For " << names[index] << " choose " << index + 1 << std::endl;
							}
							std::cin >> opt;
							unsigned gold = (*currentPlayer).ammountOfGold();
							if ((opt >= 0 && opt <= 2 && gold < 4) || (opt >= 3 && opt <= 6 && gold < 3) || (opt >= 7 && opt <= 12 && gold < 2) || (opt >= 13 && opt <= 16 && gold < 1)) {
								std::cout << "Sorry, you don't have enought gold to destroy this district!" << std::endl;
							}
							else {
								districtDestroyedByWarlord = names[opt - 1];
								warlordPlayer = currentPlayer;
							}

						}
						system("cls");
					}//end if for yes option to use ch card
					//STILL ON IF BRANCH WHERE PLAYER SELECTED BY CH CARD

					if ((*currentPlayer).hasWinningState()) {
						gameBoard.addPlayerToWinners((*currentPlayer));
						gameHasEnded = true;
					}
				}
		}

		gameBoard.reinitialiseCharacterDeck();
		if (gameHasEnded)
		{
			gameBoard.printWinners();
		}
		//GAMELOOP
	}
}

#pragma once
#include<vector>
#include<stdexcept>
#include<queue>
#include<algorithm>
#include<iterator>
#include"DistrictDeck.h"
#include"CharacterDeck.h"
#include"Player.h"

static bool comparePoints(const Player& first, const Player& second)
{
	return (first.getNumberOfPoints() < second.getNumberOfPoints());
}

class Board
{
protected:
	std::vector<Player> playerList;
	std::vector<Player> winners;
	DistrictDeck districtDeck;
	CharacterDeck characterDeck;
	Player startingPlayer;
	unsigned numberOfPlayers;

public:
     Board();
	 
public:
	 void addPlayer(const Player& player);
	 void addPlayerToWinners(const Player& player);
	 void sendGoldToPlayer(unsigned index, unsigned goldAmount);
	 void getGoldFromPlayer(unsigned index, unsigned goldAmount);
	 void sendDistrictCardToPlayer(unsigned index);
	 void sendDistrictCardBackToDeck(DistrictCard &districtCardToBePutBack);
	 void sendCharactherCardToPlayer(unsigned playerIndex, unsigned cardIndex);
	 DistrictCard  getDistrictCard();
	 CharactherCard & getCharacterCard();
	// CharactherCard & getCharacterCardFromPlayer(unsigned plIndex) const;
	 unsigned oldestPlayer();
	 std::vector<Player>::iterator playerWithCrown();
	 std::vector<Player>::iterator lastPlayerInVector();
	 std::vector<Player>::iterator firstPlayerInVector();
	 void giveCrown(unsigned index);
	 unsigned returnNumberOfPoints(Player &player, unsigned position);
	 void eliminateFirstCardFromCharacterDeck();
	 void eliminateCharacterCardByCharacterIndex(unsigned index);
	 void printCharactersDeck();
	 void printBurntCards();
	 unsigned getPlayerIndex(Player& player);
	 void firstDistrictCardInDeck(DistrictCard &emptyCard);
	 void secondDistrictCardInDeck(DistrictCard &emptyCard);
	 void removeFirstDistrictCardInDeck();
	 void removeSecondDistrictCardInDeck();
	 std::vector<DistrictCard>::iterator firstDistrictCardInDeckIterator();
	 std::vector<DistrictCard>::iterator secondDistrictCardInDeckIterator();
	 void sendCardToTheBackOfTheDistrictDeck(std::vector<DistrictCard>::iterator cardToBeSent);
	 void sendCardToTheBackOfTheDistrictDeckMagician(DistrictCard cardToBeSent);
	 void printWinners();

public:
	void initialiseDecks(unsigned numberOfPlayers);
	void reinitialiseCharacterDeck();
	void setNumberOfPlayers(unsigned number);
	void shuffleDistrictDeck();
	void shuffleCharacterDeck();

};


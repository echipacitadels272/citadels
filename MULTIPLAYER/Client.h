#include<iostream>

#include <winsock2.h>	// contains most of the Winsock functions, structures, and definitions
#include <ws2tcpip.h>	// contains newer functions and structures used to retrieve IP addresses

#pragma comment(lib, "Ws2_32.lib")	//  indicates to the linker that the Ws2_32.lib
int mainClient(int argc, char * argv[])
{
// Validate the parameters
if (argc != 2) {
	printf("usage: %s server-name\n", argv[0]);
	return 1;
}

// *** initialize Winsock ***
// initialize Winsock before making other Winsock functions calls (only once per application/dll)
WSADATA wsaData;
int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);		// initiate use of WS2_32.dll, with version 2.2
if (iResult != 0)
{
	printf("WSAStartup failed: %d\n", iResult);
	return 1;
}

// *** Create a socket ***
addrinfo *result = NULL, *ptr = NULL, hints;

ZeroMemory(&hints, sizeof(hints));	// memset to 0
hints.ai_family = AF_UNSPEC;		// unspecified: either an IPv6 or IPv4
hints.ai_socktype = SOCK_STREAM;	// stream socket
hints.ai_protocol = IPPROTO_TCP;	// TCP protocol

#define DEFAULT_PORT "27015"

	// Resolve the server address and port
iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
if (iResult != 0)
{
	printf("getaddrinfo failed: %d\n", iResult);
	WSACleanup();	// note: use WSACleanup when done working with sockets
	return 1;
}

SOCKET ConnectSocket = INVALID_SOCKET;
// Attempt to connect to the first address returned by the call to getaddrinfo
ptr = result;

// Create a SOCKET for connecting to server
ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

if (ConnectSocket == INVALID_SOCKET)
{
	printf("Error at socket(): %ld\n", WSAGetLastError());
	freeaddrinfo(result);
	WSACleanup();
	return 1;
}

// *** Connect to server ***
iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
if (iResult == SOCKET_ERROR)
{
	closesocket(ConnectSocket);
	ConnectSocket = INVALID_SOCKET;
}

// Should really try the next address returned by getaddrinfo
// if the connect call failed
// But for this simple example we just free the resources
// returned by getaddrinfo and print an error message

freeaddrinfo(result);

if (ConnectSocket == INVALID_SOCKET)
{
	printf("Unable to connect to server!\n");
	WSACleanup();
	return 1;
}

// *** Send data to server ***
#define DEFAULT_BUFLEN 512

int recvbuflen = DEFAULT_BUFLEN;

const char *sendbuf = "this is a test";
char recvbuf[DEFAULT_BUFLEN];

// Send an initial buffer
iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
//char *test = new char[100];
//strcpy(test, "weird flex but ok\n");
//iResult = send(ConnectSocket, test, (int)strlen(test), 0);
if (iResult == SOCKET_ERROR)
{
	printf("send failed: %d\n", WSAGetLastError());
	closesocket(ConnectSocket);
	WSACleanup();
	return 1;
}

printf("Bytes Sent: %ld\n", iResult);

// shutdown the connection for sending since no more data will be sent
// the client can still use the ConnectSocket for receiving data
iResult = shutdown(ConnectSocket, SD_SEND);
if (iResult == SOCKET_ERROR)
{
	printf("shutdown failed: %d\n", WSAGetLastError());
	closesocket(ConnectSocket);
	WSACleanup();
	return 1;
}

// Receive data until the server closes the connection
do
{
	iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
	if (iResult > 0)
	{
		printf("Bytes received: %d\n", iResult);
		printf("Buffer content: %.*s\n", iResult, recvbuf);
	}
	else if (iResult == 0)
		printf("Connection closed\n");
	else
		printf("recv failed: %d\n", WSAGetLastError());
} while (iResult > 0);

// *** cleanup ***
closesocket(ConnectSocket);
WSACleanup();

return 0;
}
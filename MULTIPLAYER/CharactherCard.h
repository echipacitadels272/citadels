#pragma once
#include<string>
#include"Card.h"

const int kInvalidIndex = -1;
const std::string kInvalidName = "#";


class CharactherCard :
	public Card
{
public:
	enum class Type :uint8_t
	{
		None,
		Assassin,
		Thief,
		Magician,
		King,
		Bishop,
		Merchant,
		Architect,
		Warlord
	};

public:
	CharactherCard();
	CharactherCard(unsigned int index, std::string name, Type type);
	CharactherCard(const CharactherCard& otherCard);
	//CharactherCard(CharactherCard&& otherCard);
	~CharactherCard();

	Type getType() const;
	unsigned getIndex() const;
	std::string getCharacterCardName() const;

public:
	CharactherCard& operator = (const CharactherCard &otherCard);
	//CharactherCard& operator = (CharactherCard&& otherCard);

public:
	friend std::ostream& operator << (std::ostream& output, CharactherCard& charactherCard);

private:
	unsigned m_index;
	Type m_type;
};


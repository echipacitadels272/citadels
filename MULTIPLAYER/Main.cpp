#include"Game.h"
#include"GameMultiplayer.h"
#include"Client.h"
//#include"Server.h"
#include<ctype.h>


void printSplashScreen()
{
	std::cout << "           /$$   /$$                     /$$           /$$          " << std::endl;
	std::cout << "          |__/  | $$                    | $$          | $$          " << std::endl;
	std::cout << "  /$$$$$$$ /$$ /$$$$$$    /$$$$$$   /$$$$$$$  /$$$$$$ | $$  /$$$$$$$" << std::endl;
	std::cout << " /$$_____/| $$|_  $$_/   |____  $$ /$$__  $$ /$$__  $$| $$ /$$_____/" << std::endl;
	std::cout << "| $$      | $$  | $$      /$$$$$$$| $$  | $$| $$$$$$$$| $$|  $$$$$$ " << std::endl;
	std::cout << "| $$      | $$  | $$ /$$ /$$__  $$| $$  | $$| $$_____/| $$ \____  $$" << std::endl;
	std::cout << "|  $$$$$$$| $$  |  $$$$/|  $$$$$$$|  $$$$$$$|  $$$$$$$| $$ /$$$$$$$/" << std::endl;
	std::cout << " \\_______/|__/  \\_____/  \\_______/\\_______/ \\_______/ |__/|_______/ " << std::endl;
	std::cout << std::endl;
	std::cout << "By Mihai Teodor, Robert Manea and Maner Danut" << std::endl;
	std::cout << "Transylvania University of Brasov" << std::endl;
	std::cout << "2018" << std::endl;
	std::cout << std::endl;
	system("pause");
}


int main() {

	bool isHost = false;

	printSplashScreen();
	std::string option;
	char * initialMessage;
	initialMessage = new char[50];
	char * ipAddress;
	ipAddress = new char[50];
	char ** dateHost;
	dateHost = new char *[2];
	strcat(initialMessage, "conectat la serverul host ");
	dateHost[0] = initialMessage;
	std::cout << "Select S for Single Player or M for MultiPlayer" << std::endl;
	std::cin >> option;
	while (true) {
		if (option == "S" || option == "s") {
			Game newGame;
			newGame.initGame();
			break;
		}
		else
			if (option == "M" || option == "m") {
				system("cls");
				
				std::cout << "Are you the host?\nY for yes and N for no" << std::endl;
				std::cin >> option;
				if (option == "Y" || option == "y") {
					GameMultiplayer newGame;
					system("cls");
					newGame.mainServer();
					newGame.initGame();
				}
				else {
					std::cout << "Insert host IPv4 address" << std::endl;
					std::cin >> ipAddress;
					dateHost[1] = ipAddress;
					system("cls");
				   mainClient(2, dateHost);
				}
			}
	}
	return 0;
}


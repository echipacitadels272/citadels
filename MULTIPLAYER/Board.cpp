#include "Board.h"

Board::Board()
{
	numberOfPlayers = kInvalidNumberOfPlayers;
}

void Board::addPlayer(const Player & player)
{
	playerList.push_back(player);
}

void Board::addPlayerToWinners(const Player & player)
{
	winners.push_back(player);
}

void Board::shuffleDistrictDeck()
{
	districtDeck.Shuffle();
}

void Board::shuffleCharacterDeck()
{
	characterDeck.Shuffle();
}


void Board::sendGoldToPlayer(unsigned index, unsigned goldAmount)
{
	unsigned int gold = playerList[index].getNumberOfGold();
	gold += goldAmount;
	playerList[index].setNumberOfGold(gold);
}

void Board::getGoldFromPlayer(unsigned index, unsigned goldAmount)
{
	unsigned gold = playerList[index].getNumberOfGold();
	if (gold >= goldAmount)
	{
		gold -= goldAmount;
		playerList[index].setNumberOfGold(gold);
	}
	else {
		throw std::invalid_argument("player didn't had enough gold");
	}
}

void Board::sendDistrictCardToPlayer(unsigned index)
{
	DistrictCard cardToBeSent = districtDeck.getCard();
	playerList[index].setDistrictCard(cardToBeSent);
}

void Board::sendDistrictCardBackToDeck(DistrictCard &districtCardToBePutBack)
{
	districtDeck.putCardBack(districtCardToBePutBack);
}

void Board::sendCharactherCardToPlayer(unsigned playerIndex, unsigned cardIndex)
{
	playerList[playerIndex].setindexOfCharacterCardOwned(cardIndex);
	CharactherCard card;
	characterDeck.getCertainCard(cardIndex, card);
	playerList[playerIndex].setCharacterCard(card);
	std::cout << "\npicked card: " << playerList[playerIndex].getCharactherCard().getCharacterCardName() << std::endl;
}



DistrictCard Board::getDistrictCard()
{
	DistrictCard cardToBeSent = districtDeck.getCard();
	return cardToBeSent;
}

CharactherCard & Board::getCharacterCard()
{
	return characterDeck.getCard();
}




unsigned Board::oldestPlayer()
{
	unsigned oldestIndex = 0, biggestAge = playerList[0].getAge(), index;
	for(index=1; index < numberOfPlayers; ++index)
		if (playerList[index].getAge() > biggestAge) {
			biggestAge = playerList[index].getAge();
			oldestIndex = index;
		}
	return oldestIndex;
}

std::vector<Player>::iterator Board::playerWithCrown()
{
	std::vector<Player>::iterator playerWithCrown;
	for (playerWithCrown = playerList.begin(); playerWithCrown <= playerList.end(); ++playerWithCrown)
		if ((*playerWithCrown).hasCrown())
			return playerWithCrown;
}

std::vector<Player>::iterator Board::lastPlayerInVector()
{
	return playerList.end()-1;
}

std::vector<Player>::iterator Board::firstPlayerInVector()
{
	return playerList.begin();
}


void Board::giveCrown(unsigned index)
{
	for (auto x : playerList)
		x.resetCrown();
	playerList[index].setCrown();
}

unsigned Board::returnNumberOfPoints(Player & player, unsigned position)
{
	// 1) A player receives a number of points equal to the total combined gold cost of all the district cards in his city .
	unsigned numberOfPoints = player.getGoldValueOfCitadel();

	// 2)  If a player has at least one district in each of the five colors, he receives three points. 

	if (player.deckOfFiveDistinctColors())
	{
		numberOfPoints += 3;
	}

	// 3)  If a player was the first player to build eight districts, he receives four points. 

	if (player.hasWinningState() && position == 1)
	{
		numberOfPoints += 4;
	}


	// 4)  All other players who have managed to build eight districts at the end of the game receive two points. 
	if (player.hasWinningState() && position > 1)
	{
		numberOfPoints += 2;
	}

	player.setNumberOfPoints(numberOfPoints);
	return numberOfPoints;
}

void Board::eliminateFirstCardFromCharacterDeck()
{
	characterDeck.eliminateFirstCardFromDeck();
}

void Board::eliminateCharacterCardByCharacterIndex(unsigned index)
{
	characterDeck.eliminateCertainCardFromDeck(index);
}




void Board::printCharactersDeck()
{
	characterDeck.printCards();
}

void Board::printBurntCards()
{
	characterDeck.printBurntCards();
}

unsigned Board::getPlayerIndex(Player & player)
{
	for (unsigned index = 0; index < playerList.size(); ++index)
		if (playerList[index] == player)
			return index;
}

void Board::firstDistrictCardInDeck(DistrictCard &emptyCard)
{
	emptyCard = (*districtDeck.firstCardInDeck());
}

void Board::secondDistrictCardInDeck(DistrictCard &emptyCard)
{
	emptyCard = (*districtDeck.secondCardInDeck());
}

void Board::removeFirstDistrictCardInDeck()
{
	districtDeck.eliminateFirstCardFromDeck();
}

void Board::removeSecondDistrictCardInDeck()
{
	districtDeck.eliminateSecondCardFromDeck();
}

std::vector<DistrictCard>::iterator Board::firstDistrictCardInDeckIterator()
{
	//std::cout << "test: " << (*districtDeck.firstCardInDeck()).getName() << "\n";
	return districtDeck.firstCardInDeck();
}

std::vector<DistrictCard>::iterator Board::secondDistrictCardInDeckIterator()
{
	return districtDeck.secondCardInDeck();
}

void Board::sendCardToTheBackOfTheDistrictDeck(std::vector<DistrictCard>::iterator cardToBeSent)
{
	districtDeck.sendCardToTheBackOfTheDeck(cardToBeSent);
}

void Board::sendCardToTheBackOfTheDistrictDeckMagician(DistrictCard cardToBeSent)
{
	districtDeck.sendCardToTheBackOfTheDeckMagician(cardToBeSent);
}

void Board::printWinners()
{

	for (unsigned index = 0; index < winners.size(); ++index)
	{
		unsigned points = returnNumberOfPoints(winners[index], index + 1);
		winners[index].setNumberOfPoints(points);
	}
	std::sort(winners.begin(), winners.end(), comparePoints);

	std::cout << "Winners:" << std::endl;
	std::cout << "First place: " << winners[0].getName() << " , with a total of " << winners[0].getNumberOfPoints() << std::endl;
	if (winners.size() > 1) {
		std::cout << "Additional winners:" << std::endl;
		for(unsigned index=1;index < winners.size();++index)
			std::cout << winners[index].getName() << " , with a total of " << winners[index].getNumberOfPoints() << std::endl;

	}
	
}



void Board::setNumberOfPlayers(unsigned number)
{
	numberOfPlayers = number;
}


void Board::initialiseDecks(unsigned numberOfPlayers)
{
	districtDeck.setNumberOfPlayers(numberOfPlayers);
	districtDeck.generateDistrictDeck();
	characterDeck.generateCharactherDeck();
	districtDeck.Shuffle();
	characterDeck.Shuffle();
}

void Board::reinitialiseCharacterDeck()
{
	characterDeck.generateCharactherDeck();
	//districtDeck.Shuffle();
	characterDeck.Shuffle();
}




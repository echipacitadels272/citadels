#pragma once
#include"DistrictCard.h"
#include<vector>
#include<algorithm>
#include<iostream>
#include<ctime>

const unsigned int kInvalidNumberOfPlayers = 0;

class DistrictDeck
{
public:
	DistrictDeck();
	DistrictDeck(int nrOfPlayers);
	DistrictDeck(const DistrictDeck& otherDeck);
	DistrictDeck(DistrictDeck&& otherDeck);

public:
	DistrictDeck& operator = (const DistrictDeck& otherDeck);
	DistrictDeck& operator = (DistrictDeck&& otherDeck);

public:
	void generateDistrictDeck();
	void Shuffle();
	void printCards();
	DistrictDeck getDeck();
	DistrictCard getCard();
	unsigned getDeckSize() const;
	void putCardBack(DistrictCard &cardToBePutBack);
	void insertDistrictCard(DistrictCard &cardToBeInserted);
	unsigned returnGoldValueOfDeck();
	unsigned getDeckSize();
	bool containsFiveDistrictColors();
	void eliminateFirstCardFromDeck();
	void eliminateSecondCardFromDeck();
	void sendCardToTheBackOfTheDeck(std::vector<DistrictCard>::iterator cardToBeSent);
	void sendCardToTheBackOfTheDeckMagician(DistrictCard cardToBeSent);
	unsigned goldFromNobleDistricts();
	unsigned goldFromReligiousDistricts();
	unsigned goldFromTradeDistricts();
	unsigned goldFromMilitaryDistricts();
	std::vector<DistrictCard>::iterator firstCardInDeck();
	std::vector<DistrictCard>::iterator secondCardInDeck();
	std::vector<DistrictCard>::iterator lastCardInDeck();

public:
	void setNumberOfPlayers(unsigned nrOfPlayers);

protected:
	std::vector<DistrictCard> deckCards;
	unsigned numberOfPlayers;
	DistrictCard cardToBeSent;
};



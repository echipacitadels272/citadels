#pragma once
#include<string>
#include"Card.h"

class DistrictCard: public Card
{
public:
    enum class Color :uint8_t
	{
	    None,
		Yellow,
		Blue,
		Green,
		Red,
		Purple
	};
public:
	DistrictCard();
	DistrictCard(unsigned int cost, std::string name,Color color);
	DistrictCard(const DistrictCard &other_card);
	DistrictCard(DistrictCard && other_card);

	unsigned int GetCostInGold() const;
	std::string GetDistrictCardName() const;
	Color GetColor()const;

	friend std::ostream& operator << (std::ostream& output, DistrictCard & other_card);
	
	DistrictCard & operator = (const DistrictCard & other_card);
	DistrictCard & operator = (DistrictCard && other_card);
	~DistrictCard();

private:
	unsigned int m_costInGold;
	//std::string m_name;
	Color m_color;
};


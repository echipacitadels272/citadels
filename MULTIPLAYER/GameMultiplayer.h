#pragma once
#include<queue>
#include<iostream>
#include"Board.h"
#include"CharactherCard.h"
#include"DistrictCard.h"
#include"Player.h"
#include <winsock2.h>	// contains most of the Winsock functions, structures, and definitions
#include <ws2tcpip.h>	// contains newer functions and structures used to retrieve IP addresses
#pragma comment(lib, "Ws2_32.lib")	//  indicates to the linker that the Ws2_32.lib


class GameMultiplayer
{

public:
	void initGame();
	void printMessage(const char *x, const SOCKET &ClientSocket);
	void acceptClientSocket(SOCKET &clSocket, SOCKET &lisSocket);
	void establishConnections(int numberOfPlayers, SOCKET &lisSocket);
	void shutDownConnections();
	int mainServer();

private:
	Board gameBoard;
	unsigned numberOfPlayers, goldInBank;
	const unsigned int KInitialNumberOfDisitrictCardsM = 4;
	unsigned kInvalidPlayerIndexM = 0;
	std::vector<SOCKET> playerConnections;

};